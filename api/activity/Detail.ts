import useSWR from "swr";

const GetDetailActivity = (id:any) => {
    var url = `${process.env.NEXT_PUBLIC_API_URL}/activity-groups/`;
    
    const { data, error, mutate } = useSWR( !!id ?
        url+id : null
    );

    return { data, error, mutate };
};

export default GetDetailActivity;

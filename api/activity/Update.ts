import axios from "axios";

var url = `${process.env.NEXT_PUBLIC_API_URL}/activity-groups`;

const ApiUpdateActivity = async (id: any, title: any) => {
    return axios.patch(url + "/" + id, {
        title: title,
    });
};

export default ApiUpdateActivity;

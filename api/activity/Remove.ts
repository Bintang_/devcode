import axios from "axios";

var url = `${process.env.NEXT_PUBLIC_API_URL}/activity-groups`;

const ApiDeleteActivity = async (id: number) => {
    return axios.delete(`${url}/${id}`);
};

export default ApiDeleteActivity;

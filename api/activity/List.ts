import useSWR from "swr";

const GetActivityList = () => {
    var url = `${process.env.NEXT_PUBLIC_API_URL}`;
    var email = `${process.env.NEXT_PUBLIC_EMAIL}`;
    
    const { data, error, mutate } = useSWR(
        `${url}/activity-groups?email=${email}`
    );

    return { data, error, mutate };
};

export default GetActivityList;

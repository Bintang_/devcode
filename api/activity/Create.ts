import axios from "axios";

const ApiCreateActivity = async () => {
    var url = `${process.env.NEXT_PUBLIC_API_URL}/activity-groups`;
    
    return axios.post(url, {
        title: "new task",
        email: "bintangseptiandaru@gmail.com",
    });
};

export default ApiCreateActivity;

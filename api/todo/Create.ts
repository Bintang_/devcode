import axios from "axios";

var url = `${process.env.NEXT_PUBLIC_API_URL}/todo-items`;

const ApiCreateTodo = async (pid: number, title: string, priority: any) => {
    return axios.post(url, {
        activity_group_id: pid,
        title: title,
        priority: priority,
    });
};

export default ApiCreateTodo;

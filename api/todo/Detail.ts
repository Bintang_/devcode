import useSWR from "swr";

var url = `${process.env.NEXT_PUBLIC_API_URL}/todo-items/`;

const GetDetailTodo = (id:any) => {
    const { data, error, mutate } = useSWR(
        url+id
    );

    return { data, error, mutate };
};

export default GetDetailTodo;

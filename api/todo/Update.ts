import axios from "axios";

var url = `${process.env.NEXT_PUBLIC_API_URL}/todo-items`;

export const ApiUpdateTodo = async (
    pid: number | undefined,
    group_id: number,
    title: string,
    priority: any
) => {
    return axios.patch(url + "/" + pid, {
        activity_group_id: group_id,
        title: title,
        priority: priority,
    });
};

export const ApiUpdateTodoActive = async (pid: number, active: boolean) => {
    return axios.patch(url + "/" + pid, {
        is_active: active,
    });
};

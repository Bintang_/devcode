import axios from "axios";

var url = `${process.env.NEXT_PUBLIC_API_URL}/todo-items`;

const ApiDeleteTodo = async (
    pid: number | undefined
) => {
    return pid ? axios.delete(url + "/" + pid) : null;
};

export default ApiDeleteTodo
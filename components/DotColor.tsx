import React from "react";

const DotColor = ({ priority, dataCy }: any) => {
    switch (priority) {
        case "very-high":
            return (
                <span
                    className={`w-4 h-4 aspect-square rounded-full bg-[#ED4C5C]
                `}
                    data-cy={dataCy}
                ></span>
            );
        case "high":
            return (
                <span
                    className={`w-4 h-4 aspect-square rounded-full bg-[#F8A541]
                `}
                    data-cy={dataCy}
                ></span>
            );
        case "normal":
            return (
                <span
                    className={`w-4 h-4 aspect-square rounded-full bg-[#00A790]
                `}
                    data-cy={dataCy}
                ></span>
            );
        case "low":
            return (
                <span
                    className={`w-4 h-4 aspect-square rounded-full bg-[#428BC1]
                `}
                    data-cy={dataCy}
                ></span>
            );
        case "very-low":
            return (
                <span
                    className={`w-4 h-4 aspect-square rounded-full bg-[#8942C1]
                `}
                    data-cy={dataCy}
                ></span>
            );

        default:
            return <></>;
    }
};

export default DotColor;

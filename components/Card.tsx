import React from "react";
import TrashIcon from "./icons/TrashIcon";
import moment from "moment";
import Router from "next/router";

type TCard = {
    id: number;
    title: string;
    date: string;
    action: () => any;
};

const Card = ({ id, title, date, action }: TCard) => {
    const time = moment(date).format("DD MMMM YYYY");

    return (
        <div className="rounded-lg shadow-[0_4px_8px_rgba(0,0,0,0.15)] bg-white w-full aspect-square h-full flex flex-col justify-between p-6" data-cy="activity-item">
            <p className="font-bold capitalize text-lg h-full cursor-pointer" data-cy="activity-item-title" onClick={()=> Router.push(`/detail/${id}`)}>
                {title}
            </p>
            <div className="flex w-full justify-between items-center">
                <span className="text-sysGrey" data-cy="activity-item-date">{time}</span>
                <button
                    className="p-1"
                    data-cy="activity-item-delete-button"
                    onClick={action}
                >
                    <TrashIcon />
                </button>
            </div>
        </div>
    );
};

export default Card;

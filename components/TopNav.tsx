import React from 'react'

const TopNav = ({title, children} :any) => {
  return (
    <div className='w-full flex justify-between'>
        <h1 className='font-bold text-4xl capitalize' data-cy="activity-title">{title}</h1>
        <div>{children}</div>
    </div>
  )
}

export default TopNav
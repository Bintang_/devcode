import React from "react";

const Header = () => {
    return (
        <div
            className="w-full bg-primary h-[105px] flex justify-center items-center"
            data-cy="header-background"
        >
            <div className="container max-w-[1000px] px-4">
                <h3
                    data-cy="header-title"
                    className="font-bold text-2xl uppercase text-white"
                >
                    TO DO LIST APP
                </h3>
            </div>
        </div>
    );
};

export default Header;

import React, { useState } from "react";
import Card from "./Card";
import { Modal } from "./Modal";
import ModalDeleteIcon from "./icons/ModalDeleteIcon";
import ApiDeleteActivity from "@@/api/activity/Remove";
import GetActivityList from "@@/api/activity/List";
import { toast } from "react-toastify";

const ActivityList = ({ data }: any) => {
    const [showModal, setModal] = useState<any | undefined>();
    const { mutate } = GetActivityList();

    const handleDeleteConfirm = (id: any) => {
        showModal == undefined && setModal(id);
    };

    const handleDeleteActivity = (id: number) => {
        ApiDeleteActivity(id).then(() => {
            setModal(undefined);
            mutate();
            toast.success(<div data-cy="modal-information">Activity berhasil di hapus.</div>)
        });
    };

    return (
        <>
            <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-6">
                {data.map((el: any, index: any) => {
                    return (
                        <Card
                            key={index}
                            id={el.id}
                            title={el.title}
                            date={el.created_at}
                            action={() => handleDeleteConfirm(el)}
                        />
                    );
                })}
            </div>

            <Modal
                show={!!showModal}
                width="w-1/3"
                onClose={() => setModal(undefined)}
            >
                <div className="bg-white w-full rounded-lg p-6 flex justify-center items-center flex-col gap-12" data-cy="modal-delete">
                    <ModalDeleteIcon />
                    <p className="text-center" data-cy="modal-delete-title">
                        Apakah anda yakin menghapus activity <br />{" "}
                        <span className="font-bold">“{showModal?.title}”?</span>
                    </p>
                    <div className="flex gap-4">
                        <button
                            className="bg-[#F4F4F4] rounded-full py-4 px-14 font-semibold"
                            onClick={() => setModal(undefined)}
                            data-cy="modal-delete-cancel-button"
                        >
                            Batal
                        </button>
                        <button
                            className="bg-[#ED4C5C] rounded-full py-4 px-14 font-semibold text-white"
                            onClick={() => handleDeleteActivity(showModal?.id)}
                            data-cy="modal-delete-confirm-button"
                        >
                            Hapus
                        </button>
                    </div>
                </div>
            </Modal>
        </>
    );
};

export default ActivityList;

import Image from "next/image";
import React from "react";
import Empty from "@@/public/assets/png/todo-empty-state.png";

const TodoEmpty = ({ action }: any) => {
    return (
        <div
            className="flex w-full justify-center cursor-pointer"
            data-cy="todo-empty-state"
            onClick={action}
        >
            <Image
                src={Empty}
                alt="todo-empty-state"
                width={767}
                height={490}
                data-cy="todo-empty-state"
            ></Image>
        </div>
    );
};

export default TodoEmpty;

import React from "react";

type TButton = {
    text: string;
    icon?: React.ReactNode;
    onClick: () => any;
    name: string;
    disabled?: boolean;
};

const Button = ({ text, icon, name, onClick, disabled }: TButton) => {
    return (
        <button
            onClick={onClick}
            className={`px-6 py-3 rounded-full ${
                disabled && "bg-opacity-40"
            } bg-primary flex gap-2 items-center h-max`}
            data-cy={name}
            disabled={disabled}
        >
            {icon && <span className="w-[14px] h-[14px]">{icon}</span>}
            <span className="text-white capitalize font-semibold">{text}</span>
        </button>
    );
};

export default Button;

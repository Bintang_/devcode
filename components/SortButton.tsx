import { useOnClickOutside } from "@@/hooks/useHook";
import React from "react";
import SortIcon from "./icons/SortIcon";
import SortOldIcon from "./icons/SortOldIcon";
import SortNewIcon from "./icons/SortNewIcon";
import SortAscIcon from "./icons/SortAscIcon";
import SortDescIcon from "./icons/SortDescIcon";
import SortArrowIcon from "./icons/SortArrowIcon";

const SortButton = (props: any) => {
    const ref = React.useRef<HTMLDivElement>(null);

    const [sort, setSort] = props.State;
    const [searchOption, setSearchOption] = React.useState(false);

    useOnClickOutside(ref, () => {
        if (searchOption) {
            setSearchOption(false);
        }
    });

    const handleClick = (value: any) => {
        setSort(value);
        setSearchOption(false);
    };

    return (
        <div className="relative">
            <button
                onClick={() => setSearchOption(!searchOption)}
                data-cy="todo-sort-button"
            >
                <SortIcon />
            </button>
            {searchOption && (
                <div
                    ref={ref}
                    className="absolute w-max divide-y bg-white rounded-lg border flex flex-col bottom-o -left-0"
                >
                    <button
                        className="w-full py-3 px-6 hover:bg-gray-400 flex gap-4 rounded-t-lg"
                        onClick={() => handleClick("new")}
                        data-cy="sort-selection"
                    >
                        <SortOldIcon data-cy="sort-selection-icon" />
                        <span
                            className="w-full text-left"
                            data-cy="sort-selection-title"
                        >
                            Terbaru
                        </span>
                        <div>{sort == "new" && "V"}</div>
                    </button>
                    <button
                        className="w-full py-3 px-6 hover:bg-gray-400 flex gap-4"
                        onClick={() => handleClick("old")}
                        data-cy="sort-selection"
                    >
                        <SortNewIcon data-cy="sort-selection-icon" />
                        <span
                            className="w-full text-left"
                            data-cy="sort-selection-title"
                        >
                            Terlama
                        </span>
                        <div>{sort == "old" && "V"}</div>
                    </button>
                    <button
                        className="w-full py-3 px-6 hover:bg-gray-400 flex gap-4"
                        onClick={() => handleClick("asc")}
                        data-cy="sort-selection"
                    >
                        <SortAscIcon data-cy="sort-selection-icon" />
                        <span
                            className="w-full text-left"
                            data-cy="sort-selection-title"
                        >
                            A-Z
                        </span>
                        <div>{sort == "asc" && "V"}</div>
                    </button>
                    <button
                        className="w-full py-3 px-6 hover:bg-gray-400 flex gap-4"
                        onClick={() => handleClick("desc")}
                        data-cy="sort-selection"
                    >
                        <SortDescIcon data-cy="sort-selection-icon" />
                        <span
                            className="w-full text-left"
                            data-cy="sort-selection-title"
                        >
                            Z-A
                        </span>
                        <div>{sort == "desc" && "V"}</div>
                    </button>
                    <button
                        className="w-full py-3 px-6 hover:bg-gray-400 flex gap-4 rounded-b-lg"
                        onClick={() => handleClick("unfinish")}
                        data-cy="sort-selection"
                    >
                        <SortArrowIcon data-cy="sort-selection-icon" />
                        <span
                            className="w-full text-left"
                            data-cy="sort-selection-title"
                        >
                            Belum Selesai
                        </span>
                        <div>{sort == "unfinish" && "V"}</div>
                    </button>
                </div>
            )}
        </div>
    );
};

export default SortButton;

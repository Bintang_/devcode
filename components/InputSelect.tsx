import React from "react";
import { ColourOption, colourOptions } from "@@/data";
import Select, { OptionProps, StylesConfig, components } from "react-select";

const dot = (color = "transparent") => ({
    alignItems: "center",
    display: "flex",

    ":before": {
        backgroundColor: color,
        borderRadius: 10,
        content: '" "',
        display: "block",
        marginRight: 8,
        height: 10,
        width: 10,
    },
});

const colourStyles: StylesConfig<ColourOption> = {
    option: (styles, { data }) => ({ ...styles, ...dot(data.color) }),
    singleValue: (styles, { data }) => ({ ...styles, ...dot(data.color) }),
};

const CustomOption = (props: OptionProps<any, false>) => {
    return (
        <components.Option {...props}>
            <div data-cy="modal-add-priority-item" key={props.innerProps.key}>
                {props.data.label}
            </div>
        </components.Option>
    );
};

const CustomChevron = (props: any) => {
    return (
        <components.IndicatorsContainer {...props}>
            <div
                data-cy="modal-add-priority-dropdown"
                key={props.innerProps.key}
                className="px-4 text-gray-600"
            >
                V
            </div>
        </components.IndicatorsContainer>
    );
};

const InputSelect = ({ priority, action }: any) => {
    return (
        <Select
            className="w-full md:w-1/4"
            components={{
                Option: CustomOption,
                DropdownIndicator: CustomChevron,
            }}
            options={colourOptions}
            styles={colourStyles}
            defaultValue={colourOptions.filter(
                (el: any) => el.value == priority
            )}
            onChange={action}
        />
    );
};

export default InputSelect;

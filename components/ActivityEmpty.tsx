import Image from "next/image";
import React from "react";
import Empty from "@@/public/assets/png/activity-empty-state.png";

const ActivityEmpty = ({ action }: any) => {
    return (
        <div
            className="flex w-full justify-center cursor-pointer"
            data-cy="activity-empty-state"
            onClick={action}
        >
            <Image
                src={Empty}
                alt="activity-empty-state"
                width={767}
                height={490}
                data-cy="activity-empty-state"
            ></Image>
        </div>
    );
};

export default ActivityEmpty;

import axios from "axios";
import ApiCreateActivity from "../../../api/activity/Create";

jest.mock('axios');

describe('ApiCreateActivity', () => {
    const email = 'bintangseptiandaru@gmail.com';
    const status = {"status": 201};

    it('returns HTTP status code 201 from axios post request', async () => {
        axios.post.mockResolvedValue(status);

        const result = await ApiCreateActivity();

        expect(axios.post).toHaveBeenCalledWith(`https://todo.api.devcode.gethired.id/activity-groups`, {
            title: 'new task',
            email
        });
        expect(result).toEqual(status);
    });
});

import useSWR from "swr";
import GetDetailActivity from "../../../api/activity/Detail";

jest.mock("swr");

describe("GetDetailActivity", () => {
    const url = "https://todo.api.devcode.gethired.id";
    const id = 59452;
    const data = {
        id: 59452,
        title: "new task",
        created_at: "2023-04-17T07:21:57.000Z",
        todo_items: [],
    };
    const error = new Error("Something went wrong");
    const mutate = jest.fn();

    beforeEach(() => {
        useSWR.mockReturnValue({
            data,
            error,
            mutate,
        });
    });

    it("returns data, error, and mutate from useSWR", () => {
        const result = GetDetailActivity(id);

        expect(result).toEqual({ data, error, mutate });
    });

    it("calls useSWR with the correct URL", () => {
        GetDetailActivity(id);

        expect(useSWR).toHaveBeenCalledWith(
            `${url}/activity-groups/${id}`
        );
    });
});

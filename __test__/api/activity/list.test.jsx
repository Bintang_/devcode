import useSWR from "swr";
import GetActivityList from "../../../api/activity/List";

jest.mock("swr");

describe("GetActivityList", () => {
    const email = "bintangseptiandaru@gmail.com";
    const url = "https://todo.api.devcode.gethired.id";
    const data = {
        total: 1,
        limit: 1000,
        skip: 0,
        data: [
            {
                id: 59452,
                title: "new task",
                created_at: "2023-04-17T07:21:57.000Z",
            },
        ],
    };
    const error = new Error("Something went wrong");
    const mutate = jest.fn();

    beforeEach(() => {
        useSWR.mockReturnValue({
            data,
            error,
            mutate,
        });
    });

    it("returns data, error, and mutate from useSWR", () => {
        const result = GetActivityList();

        expect(result).toEqual({ data, error, mutate });
    });

    it("calls useSWR with the correct URL", () => {
        GetActivityList(email, url);

        expect(useSWR).toHaveBeenCalledWith(
            `${url}/activity-groups?email=${email}`
        );
    });
});

import axios from "axios";
import ApiDeleteActivity from "../../../api/activity/Remove";

jest.mock("axios");

describe("ApiCreateActivity", () => {
    const id = 59458
    const status = { status: 200 };

    it("returns HTTP status code 200 from axios delete request", async () => {
        axios.delete.mockResolvedValue(status);

        const result = await ApiDeleteActivity(id);

        expect(axios.delete).toHaveBeenCalledWith(
            `https://todo.api.devcode.gethired.id/activity-groups/${id}`
        );
        expect(result).toEqual(status);
    });
});

import axios from "axios";
import ApiUpdateActivity from "../../../api/activity/Update";

jest.mock("axios");

describe("ApiCreateActivity", () => {
    const id = 59458
    const title = "test"
    const status = { status: 200 };

    it("returns HTTP status code 200 from axios patch request", async () => {
        axios.patch.mockResolvedValue(status);

        const result = await ApiUpdateActivity(id, title);

        expect(axios.patch).toHaveBeenCalledWith(
            `https://todo.api.devcode.gethired.id/activity-groups/${id}`, {
                title:"test"
            }
        );
        expect(result).toEqual(status);
    });
});

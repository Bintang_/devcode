import axios from "axios";
import ApiDeleteTodo from "../../../api/todo/Delete";

jest.mock("axios");

describe("ApiCreateActivity", () => {
    const id = 43527
    const status = { status: 200 };

    it("returns HTTP status code 200 from axios delete request", async () => {
        axios.delete.mockResolvedValue(status);

        const result = await ApiDeleteTodo(id);

        expect(axios.delete).toHaveBeenCalledWith(
            `https://todo.api.devcode.gethired.id/todo-items/${id}`
        );
        expect(result).toEqual(status);
    });
});

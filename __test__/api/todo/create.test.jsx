import axios from "axios";
import ApiCreateTodo from "../../../api/todo/Create";

jest.mock("axios");

describe("ApiCreateTodo", () => {
    const pid = 59452;
    const title = "test";
    const priority = "high";
    const status = { status: 201 };

    it("returns HTTP status code 201 from axios post request", async () => {
        axios.post.mockResolvedValue(status);

        const result = await ApiCreateTodo(pid, title, priority);

        expect(axios.post).toHaveBeenCalledWith(
            `https://todo.api.devcode.gethired.id/todo-items`,
            {
                activity_group_id: pid,
                title,
                priority,
            }
        );
        expect(result).toEqual(status);
    });
});

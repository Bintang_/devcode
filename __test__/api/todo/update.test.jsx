import axios from "axios";
import { ApiUpdateTodo } from "../../../api/todo/Update";

jest.mock("axios");

describe("ApiCreateActivity", () => {
    const pid = 43535;
    const groupID = 59458;
    const title = "test";
    const priority = "high";
    const status = { status: 200 };

    it("returns HTTP status code 200 from axios patch request", async () => {
        axios.patch.mockResolvedValue(status);

        const result = await ApiUpdateTodo(pid, groupID, title, priority);

        expect(axios.patch).toHaveBeenCalledWith(
            `https://todo.api.devcode.gethired.id/todo-items/${pid}`,
            {
                activity_group_id: groupID,
                title,
                priority,
            }
        );
        expect(result).toEqual(status);
    });
});

import useSWR from "swr";
import GetDetailTodo from "../../../api/todo/Detail";

jest.mock("swr");

describe("GetDetailActivity", () => {
    const url = "https://todo.api.devcode.gethired.id";
    const id = 43527;
    const data = {
        "id": 43527,
        "title": "test",
        "is_active": 1,
        "priority": "very-high"
    };
    const error = new Error("Something went wrong");
    const mutate = jest.fn();

    beforeEach(() => {
        useSWR.mockReturnValue({
            data,
            error,
            mutate,
        });
    });

    it("returns data, error, and mutate from useSWR", () => {
        const result = GetDetailTodo(id);

        expect(result).toEqual({ data, error, mutate });
    });

    it("calls useSWR with the correct URL", () => {
        GetDetailTodo(id);

        expect(useSWR).toHaveBeenCalledWith(
            `${url}/todo-items/${id}`
        );
    });
});

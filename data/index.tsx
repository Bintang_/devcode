export interface ColourOption {
    readonly value: string;
    readonly label: string;
    readonly color: string;
    readonly isFixed?: boolean;
    readonly isDisabled?: boolean;
}

export const colourOptions: readonly ColourOption[] = [
    { value: "very-high", label: "Very High", color: "#ED4C5C", isFixed: true },
    { value: "high", label: "High", color: "#F8A541", isFixed: true },
    { value: "normal", label: "Medium", color: "#00A790", isFixed: true },
    { value: "low", label: "Low", color: "#428BC1", isFixed: true },
    { value: "very-low", label: "Very Low", color: "#8942C1", isFixed: true },
];

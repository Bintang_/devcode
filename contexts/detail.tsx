import GetDetailActivity from "@@/api/activity/Detail";
import ApiUpdateActivity from "@@/api/activity/Update";
import ApiCreateTodo from "@@/api/todo/Create";
import ApiDeleteTodo from "@@/api/todo/Delete";
import { ApiUpdateTodo, ApiUpdateTodoActive } from "@@/api/todo/Update";
import { colourOptions } from "@@/data";
import React, { createContext, useCallback, useEffect, useState } from "react";
import { toast } from "react-toastify";

type TObj = {
    state: any;
    data: any;
    action: any;
};

export const TodoDetailContext = createContext<TObj | null>(null);
export const TodoDetailProvider = ({ children, pid }: any) => {
    const { data, mutate, error } = GetDetailActivity(pid);

    const [modalShow, setModal] = useState<false | "input" | "delete">(false);
    const [sort, setSort] = useState<
        "new" | "old" | "asc" | "desc" | "unfinish"
    >("new");

    const [todos, setTodos] = useState([]);
    const [editTitle, setEditTitle] = useState<boolean>(false);

    const [priority, setPriority] =
        useState<typeof colourOptions[number]["value"]>("very-high");
    const [listName, setListName] = useState<string | undefined>();
    const [editID, setEditID] = useState<number>();

    const handleTitleChange = (value: any) => {
        ApiUpdateActivity(pid, value).then(() => {
            mutate();
            setEditTitle(false);
        });
    };

    const handleAddTodo = () => {
        if (listName) {
            ApiCreateTodo(parseInt(pid), listName, priority).then(() => {
                mutate();
                modalHandler(false);
            });
        } else {
            toast.warning(
                <div data-cy="modal-information">
                    Please fill title. title can not blank.
                </div>
            );
        }
    };

    const handleEditTodo = () => {
        if (listName) {
            ApiUpdateTodo(editID, parseInt(pid), listName, priority).then(
                () => {
                    mutate();
                    modalHandler(false);
                }
            );
        } else {
            toast.warning(
                <div data-cy="modal-information">
                    Please fill title. title can not blank.
                </div>
            );
        }
    };

    const handleActiveTodo = (id: number, value: boolean) => {
        ApiUpdateTodoActive(id, value).then(() => {
            mutate();
        });
    };

    const handleDeleteTodo = () => {
        ApiDeleteTodo(editID).then(() => {
            mutate();
            modalHandler(false);
            toast.success(
                <div data-cy="modal-information">
                    Item List berhasil di hapus.
                </div>
            );
        });
    };

    const modalHandler = (val: any) => {
        setModal(val);
        if (!modalShow) {
            setEditID(undefined);
            setListName(undefined);
            setEditTitle(false);
            setPriority("very-high");
        }
    };

    const sortTodos = useCallback(
        (data: any) => {
            const collator = new Intl.Collator("en", {
                numeric: true,
                sensitivity: "base",
            });

            switch (sort) {
                case "new":
                    return setTodos(data.sort((a: any, b: any) => a.id < b.id));
                case "old":
                    return setTodos(
                        data.reverse((a: any, b: any) => a.id < b.id)
                    );
                case "asc":
                    return setTodos(
                        data.sort((a: any, b: any) =>
                            collator.compare(a.title, b.title)
                        )
                    );
                case "desc":
                    return setTodos(
                        data.reverse((a: any, b: any) =>
                            collator.compare(a.title, b.title)
                        )
                    );
                case "unfinish":
                    return setTodos(
                        data.reverse((a: any, b: any) => {
                            return collator.compare(a.title, b.title);
                        })
                    );

                default:
                    return setTodos(data);
            }
        },
        [sort]
    );

    useEffect(() => {
        if (data) {
            sortTodos(data.todo_items);
        }
    }, [data, sortTodos]);

    const ctxData = {
        state: {
            modalShow: [modalShow, setModal],
            sort: [sort, setSort],
            todos: [todos, setTodos],
            editTitle: [editTitle, setEditTitle],
            priority: [priority, setPriority],
            listName: [listName, setListName],
            editID: [editID, setEditID],
        },
        data: { data, mutate, error },
        action: {
            handleTitleChange,
            handleAddTodo,
            handleEditTodo,
            handleActiveTodo,
            handleDeleteTodo,
            modalHandler,
        },
    };

    return (
        <TodoDetailContext.Provider value={ctxData}>
            {children}
        </TodoDetailContext.Provider>
    );
};

/* eslint-disable @next/next/no-img-element */
import Button from "@@/components/Button";
import TopNav from "@@/components/TopNav";
import Image from "next/image";
import Plus from "@@/public/assets/svg/plus.svg";
import ActivityEmpty from "@@/components/ActivityEmpty";
import ActivityList from "@@/components/ActivityList";
import GetActivityList from "@@/api/activity/List";
import ApiCreateActivity from "@@/api/activity/Create";

export default function Home() {
    const { data, mutate } = GetActivityList();

    const handleAddActivity = () => {
        ApiCreateActivity()
            .then(() => {
                mutate();
            })
            .catch((err) => {
                console.log(err.response.data.message);
            });
    };

    if (!data) return <>Please wait Loading</>;

    return (
        <>
            <TopNav title="Activity">
                <Button
                    name="activity-add-button"
                    icon={
                        <Image
                            src={Plus}
                            alt="tabler-plus"
                            height={24}
                            width={24}
                        />
                    }
                    text="Tambah"
                    onClick={() => handleAddActivity()}
                />
            </TopNav>
            {data.data.length == 0 ? (
                <ActivityEmpty action={() => handleAddActivity()}/>
            ) : (
                <ActivityList data={data.data} />
            )}
        </>
    );
}

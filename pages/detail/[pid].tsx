import Button from "@@/components/Button";
import TodoEmpty from "@@/components/TodoEmpty";
import ChevronLeftIcon from "@@/components/icons/ChevronLeftIcon";
import PencilIcon from "@@/components/icons/PencilIcon";
import Image from "next/image";
import Router, { useRouter } from "next/router";
import React, {
    Fragment,
    useContext,
} from "react";
import Plus from "@@/public/assets/svg/plus.svg";
import { Modal } from "@@/components/Modal";
import DotColor from "@@/components/DotColor";
import TrashIcon from "@@/components/icons/TrashIcon";
import ModalDeleteIcon from "@@/components/icons/ModalDeleteIcon";
import InputSelect from "@@/components/InputSelect";
import SortButton from "@@/components/SortButton";
import { TodoDetailContext, TodoDetailProvider } from "@@/contexts/detail";

const DetailActivity = () => {
    const router = useRouter();
    const { pid }: any = router.query;

    return (
        <TodoDetailProvider pid={pid}>
            <DetailContent />
        </TodoDetailProvider>
    );
};

const DetailContent = () => {
    const ctx = useContext(TodoDetailContext);

    const { data } = ctx?.data;

    const [, setModal] = ctx?.state.modalShow;
    const [sort, setSort] = ctx?.state.sort;

    const [todos] = ctx?.state.todos;
    const [editTitle, setEditTitle] = ctx?.state.editTitle;
    const [, setPriority] = ctx?.state.priority;
    const [, setListName] = ctx?.state.listName;
    const [, setEditID] = ctx?.state.editID;

    const { handleTitleChange, handleActiveTodo, modalHandler } = ctx?.action;

    if (!data) return <>Please wait, loading ...</>;
    return (
        <>
            <div className="space-y-8">
                <div className="w-full flex justify-between select-none items-center gap-4">
                    <div className="w-full space-x-6 flex items-center py-2">
                        <div
                            className="cursor-pointer"
                            data-cy="todo-back-button"
                            onClick={() => Router.push("/")}
                        >
                            <ChevronLeftIcon />
                        </div>
                        {!editTitle ? (
                            <h1
                                className="w-max text-4xl font-bold capitalize cursor-text"
                                data-cy="todo-title"
                                onClick={() => setEditTitle(true)}
                            >
                                {data.title}
                            </h1>
                        ) : (
                            <input
                                type="text"
                                className="outline-none focus:border-b m-0 p-0 text-4xl font-bold capitalize w-max bg-transparent"
                                defaultValue={data.title}
                                autoFocus={true}
                                onBlur={(e) =>
                                    handleTitleChange(e.target.value)
                                }
                                key={data.title}
                            />
                        )}
                        <div
                            onClick={() => setEditTitle(!editTitle)}
                            data-cy="todo-title-edit-button"
                        >
                            <PencilIcon />
                        </div>
                    </div>
                    <SortButton State={[sort, setSort]} />
                    <Button
                        icon={
                            <Image
                                src={Plus}
                                alt="tabler-plus"
                                height={24}
                                width={24}
                            />
                        }
                        text="Tambah"
                        onClick={() => modalHandler("input")}
                        name="todo-add-button"
                    />
                </div>
                <div className="flex flex-col gap-4 w-full justify-center">
                    {data && todos.length == 0 ? (
                        <TodoEmpty action={() => modalHandler("input")} />
                    ) : (
                        todos &&
                        todos.map((el: any, index: any) => {
                            return (
                                <Fragment key={index}>
                                    <div
                                        className="bg-white w-full rounded-xl shadow-xl p-8 flex gap-8 items-center"
                                        data-cy="todo-item"
                                    >
                                        <input
                                            type="checkbox"
                                            className="w-5 h-5 accent-blue-400 focus:ring-4"
                                            checked={!el.is_active}
                                            onChange={(e) =>
                                                handleActiveTodo(
                                                    el.id,
                                                    !e.target.checked
                                                )
                                            }
                                            data-cy="todo-item-checkbox"
                                        />
                                        <DotColor
                                            priority={el.priority}
                                            dataCy="todo-item-priority-indicator"
                                        />
                                        <div className="w-full flex gap-4">
                                            <span
                                                className={`capitalize text-lg ${
                                                    !el.is_active &&
                                                    "line-through text-gray-500"
                                                }`}
                                                data-cy="todo-item-title"
                                            >
                                                {el.title}
                                            </span>
                                            <button
                                                onClick={() => {
                                                    setEditID(el.id);
                                                    setListName(el.title);
                                                    setPriority(el.priority);
                                                    setModal("input");
                                                }}
                                                data-cy="todo-item-edit-button"
                                            >
                                                <PencilIcon />
                                            </button>
                                        </div>
                                        <button
                                            className=""
                                            onClick={() => {
                                                setModal("delete");
                                                setEditID(el.id);
                                                setListName(el.title);
                                            }}
                                            data-cy="todo-item-delete-button"
                                        >
                                            <TrashIcon />
                                        </button>
                                    </div>
                                </Fragment>
                            );
                        })
                    )}
                </div>
            </div>

            <ModalFormTodo />
            <ModalDelete />
        </>
    );
};

export default DetailActivity;

const ModalFormTodo = () => {
    const ctx = useContext(TodoDetailContext);

    const [modalShow] = ctx?.state.modalShow;

    const [priority, setPriority] = ctx?.state.priority;
    const [listName, setListName] = ctx?.state.listName;
    const [editID] = ctx?.state.editID;

    const { handleAddTodo, handleEditTodo, modalHandler } = ctx?.action;

    return (
        <Modal
            show={modalShow == "input"}
            width="w-full lg:w-1/2 p-8"
            onClose={() => modalHandler(false)}
        >
            <div
                className="w-full bg-white rounded-lg divide-y"
                data-cy="modal-add"
            >
                <div className="p-8 flex items-center justify-between w-full">
                    <div className="text-lg font-semibold">
                        Tambah List Item
                    </div>
                    <button onClick={() => modalHandler(false)}>X</button>
                </div>
                <div className="p-8 space-y-6">
                    <div className="space-y-1">
                        <div className="text-sm font-semibold">
                            NAMA LIST ITEM
                        </div>
                        <input
                            type="text"
                            className="rounded-lg p-4 w-full outline-none border focus:border-primary"
                            placeholder="Tambahkan nama list item"
                            onChange={(e) => setListName(e.target.value)}
                            defaultValue={listName}
                            data-cy="modal-add-name-input"
                        />
                    </div>
                    <div className="space-y-1">
                        <div className="text-sm font-semibold">PRIORITY</div>
                        <InputSelect
                            action={(e: any) => setPriority(e.value)}
                            priority={priority}
                        />
                    </div>
                </div>
                <div className="p-8 py-4 flex justify-end w-full">
                    <Button
                        name="modal-add-save-button"
                        text="Simpan"
                        onClick={() =>
                            editID ? handleEditTodo() : handleAddTodo()
                        }
                        disabled={listName == undefined ? true : false}
                    />
                </div>
            </div>
        </Modal>
    );
};

const ModalDelete = () => {
    const ctx = useContext(TodoDetailContext);

    const [modalShow, setModal] = ctx?.state.modalShow;
    const [listName] = ctx?.state.listName;

    const { handleDeleteTodo } = ctx?.action;

    return (
        <Modal
            show={modalShow == "delete"}
            width="w-1/3"
            onClose={() => setModal(false)}
        >
            <div
                className="bg-white w-full rounded-lg p-6 flex justify-center items-center flex-col gap-12"
                data-cy="modal-delete"
            >
                <ModalDeleteIcon />
                <p className="text-center" data-cy="modal-delete-title">
                    Apakah anda yakin menghapus list item <br />{" "}
                    <span className="font-bold">“{listName}”?</span>
                </p>
                <div className="flex gap-4">
                    <button
                        className="bg-[#F4F4F4] rounded-full py-4 px-14 font-semibold"
                        onClick={() => setModal(false)}
                    >
                        Batal
                    </button>
                    <button
                        className="bg-[#ED4C5C] rounded-full py-4 px-14 font-semibold text-white"
                        onClick={() => handleDeleteTodo()}
                        data-cy="modal-delete-confirm-button"
                    >
                        Hapus
                    </button>
                </div>
            </div>
        </Modal>
    );
};

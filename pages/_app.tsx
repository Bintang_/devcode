import Header from "@@/components/Header";
import "@@/styles/globals.css";
import type { AppProps } from "next/app";
import { SWRConfig } from "swr";
import axios from "axios";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const fetcher = (url: string) => axios.get(url, {}).then((res) => res.data);

export default function App({ Component, pageProps }: AppProps) {
    return (
        <>
            <SWRConfig
                value={{
                    // refreshInterval: 5 * 60 * 1000,
                    revalidateOnFocus: true,
                    fetcher: fetcher,
                }}
            >
                <Header />
                <div className="flex justify-center w-full">
                    <div className="container py-6 space-y-12 max-w-[1000px] p-4">
                        <Component {...pageProps} />
                    </div>
                </div>
            </SWRConfig>
            <ToastContainer position="top-center" hideProgressBar={true}/>
        </>
    );
}
